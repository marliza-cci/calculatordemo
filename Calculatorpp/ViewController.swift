//
//  ViewController.swift
//  Calculatorpp
//
//  Created by CCITESTING on 28/09/15.
//  Copyright © 2015 CreativeCapsule. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var firstNumTextField: UITextField!
    @IBOutlet var secondNumTextField: UITextField!
    @IBOutlet var resultTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func addNumbers(sender: AnyObject) {
       let result = self.add(NSString(string: self.firstNumTextField.text!).doubleValue , param2: NSString(string: self.secondNumTextField.text!).doubleValue)
        self.resultTextField.text = "\(result)"
    }

    @IBAction func subtractNumbers(sender: AnyObject){
        let result = self.subtract(NSString(string: self.firstNumTextField.text!).doubleValue , param2: NSString(string: self.secondNumTextField.text!).doubleValue)
        self.resultTextField.text = "\(result)"

    }
    
    @IBAction func multiplyNumbers(sender: AnyObject) {
        let result = self.multiply(NSString(string: self.firstNumTextField.text!).doubleValue , param2: NSString(string: self.secondNumTextField.text!).doubleValue)
        self.resultTextField.text = "\(result)"

    }

    @IBAction func divideNumbers(sender: AnyObject) {
        let result = self.divide(NSString(string: self.firstNumTextField.text!).doubleValue , param2: NSString(string: self.secondNumTextField.text!).doubleValue)
        self.resultTextField.text = "\(result)"

    }
    
    func add(param1: Double, param2: Double) -> Double{
        
        return 0;
    }
 
    func subtract(param1: Double, param2: Double) -> Double{
        
        return 1;
    }
    func multiply(param1: Double, param2: Double) -> Double{
        
        return 2;
    }
    func divide(param1: Double, param2: Double) -> Double{
        
        return 3;
    }
}

